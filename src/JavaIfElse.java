import java.util.Scanner;

/**
 * 
 */

/**
 * @author durgeshsingh
 *
 */
public class JavaIfElse {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();

		if (number % 2 == 1) {
			System.out.println("Weird");
		} else if (number >= 2 && number < 5) {
			System.out.println("Not Weird");
		} else if (number > 5 && number <= 20) {
			System.out.println("Weird");
		} else if (number > 20) {
			System.out.println("Not Weird");
		}
		sc.close();

	}

}
