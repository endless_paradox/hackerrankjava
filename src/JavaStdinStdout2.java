import java.util.Scanner;

/**
 * 
 */

/**
 * @author durgeshsingh
 *
 */
public class JavaStdinStdout2 {

	public static void main(String[] args) {
		
		int a;
		double b;
		String c;
		
		Scanner sc = new Scanner(System.in);
		
		a = sc.nextInt();
		b = sc.nextDouble();
		sc.nextLine();
		c = sc.nextLine();
		
		System.out.println("String: " + c + "\nDouble: " + b + "\nInt: " + a);
		sc.close();
		
	}
	
}
