import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 
 */

/**
 * @author durgeshsingh
 *
 */
public class JavaLooping {

	public static void main(String[] args) throws IOException {

		int a;

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		a = Integer.parseInt(bufferedReader.readLine().trim());
		for (int i = 1; i <= 10; i++) {
			System.out.println(a + " x " + i + " = " + a * i);
		}
		bufferedReader.close();

	}

}
