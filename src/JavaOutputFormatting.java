import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 */

/**
 * @author durgeshsingh
 *
 */
public class JavaOutputFormatting {

	public static void main(String[] args) {
		
		
		
		Scanner sc = new Scanner(System.in);
		
		List<String> strings = new ArrayList<String>();
		
		List<Integer> integers = new ArrayList<Integer>();
		
		for(int i = 0;i<3; i++)	{
			String s;
			int a;
			
			s = sc.next();
			
			a = sc.nextInt();
			
			strings.add(s);
			integers.add(a);
		}
		
		System.out.println("================================");
		
		for(int i = 0;i<3; i++)	{
			
			
			String formattedInteger = String.format("%03d", integers.get(i));
			
			String formattedString = String.format("%-" + 15 + "s" , strings.get(i));
			
			System.out.println(formattedString + formattedInteger);
			
		}
		
		sc.close();
		
		System.out.println("================================");
		
	}
	
}
