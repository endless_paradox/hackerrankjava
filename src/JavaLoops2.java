import java.util.Scanner;

/**
 * 
 */

/**
 * @author durgeshsingh
 *
 */
public class JavaLoops2 {

	public static void main(String[] args) {
		
		int q;
		Scanner sc = new Scanner(System.in);
		q = sc.nextInt();
		int previous;
		
		do	{
			
			int a, b, c;
			
			a = sc.nextInt();
			b = sc.nextInt();
			c = sc.nextInt();
			
			previous = a;
			
			for(int i = 0; i < c; i++)	{
				
				previous = previous + (int)(Math.pow(2.0, (double)i))*b;
				System.out.print(previous + " ");
				
			}
			
			System.out.println();
			
			q--;
		} while(q != 0);
		
		sc.close();
		
	}
	
}
